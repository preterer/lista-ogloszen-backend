package com.nofluffjobs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpServerResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Posting {
    private String title;
    private String company;
    private String city;
    private String street;
    private String postalCode;
    private Integer salaryMin;
    private Integer salaryMax;

    @Override
    public String toString() {
        return "Posting{" +
                "title='" + title + '\'' +
                ", company='" + company + '\'' +
                ", salaryMin=" + salaryMin +
                ", salaryMax=" + salaryMax +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }


}

