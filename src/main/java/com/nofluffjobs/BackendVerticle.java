package com.nofluffjobs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Function;

public class BackendVerticle extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        Vertx vertx = Vertx.vertx();
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());
        router.route().handler(routingContext -> {
            routingContext.response().putHeader("Access-Control-Allow-Origin", "*");
            routingContext.next();
        });
        createOptionsRoute(router);
        createGetPostingsRoute(router);
        createSavePostingRoute(router);

        server.requestHandler(router::accept).listen(8080);
    }

    private void createOptionsRoute(final Router router) {
        router.options().handler(routingContext -> {
            routingContext.response()
                    .putHeader("Access-Control-Allow-Methods", "GET, POST")
                    .putHeader("Access-Control-Allow-Headers", "content-type")
                    .setStatusCode(200)
                    .end();
        });
    }

    private void createGetPostingsRoute(final Router router) {
        router.get("/posting").handler(routingContext -> {
            getPostings(vertx, postings -> {
                routingContext.response()
                        .setChunked(true)
                        .write(new Gson().toJson(postings))
                        .setStatusCode(200)
                        .end();
            });
        });
    }

    private void createSavePostingRoute(final Router router) {
        router.post("/posting")
                .handler(routingContext -> {
                    final HttpServerResponse response = routingContext.response();
                    final Posting posting = new Gson().fromJson(routingContext.getBodyAsString(), Posting.class);
                    if (posting != null) {
                        savePosting(vertx, posting, response);
                    } else {
                        response.setStatusCode(400).end();
                    }
                });
    }

    private void savePosting(final Vertx vertx, final Posting posting, final HttpServerResponse response) {
        getPostings(vertx, postings -> {
            postings.add(posting);
            vertx.fileSystem().writeFile("postings.txt", Buffer.buffer(new Gson().toJson(postings)), result -> {
                if (result.succeeded()) {
                    response.setStatusCode(200).end();
                } else {
                    response.setStatusCode(400).end();
                }
            });
        });
    }

    private void getPostings(final Vertx vertx, final Handler<List<Posting>> handler) {
        final FileSystem fileSystem = vertx.fileSystem();
        fileSystem.readFile("postings.txt", bufferAsyncResult -> {
            if(bufferAsyncResult.succeeded()) {
                final Type listType = new TypeToken<List<Posting>>(){}.getType();
                handler.handle(new Gson().fromJson(bufferAsyncResult.result().toString(), listType));
            }
        });
    }

}
